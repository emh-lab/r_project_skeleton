# Template for an R project

## Basic workflow of data analysis

Not every data analysis is the same and but this is a useful template
for what the pieces of a data analysis are and how they flow together.

-   Define question
-   Determine what data you can access
-   Obtaining the data
-   Cleaning the data
-   Exploratory data analysis
-   Statistical prediction/modeling
-   Interpretation of results
-   Challenging of results
-   Synthesis and write up
-   Creating reproducible code

## Creating a project skeleton

Create `New Project` with `RStudio` and then create the subfolders

``` r
library(purrr)
library(here)

folder_names <- c("data/raw", "data/tidy", "doc", "R", "analysis", "figures", "output")
#sapply(folder_names, dir.create) # base R way
map(folder_names, dir.create) #purrr-fect way
```
or clone this directory using the terminal `git clone https://gitlab.ifremer.fr/emh-lab/r_project_skeleton.git`


``` r
name_of_project
|--data
    |--raw
        |--file_001.xlsx
        |--file_002.gen
        |--file_002.sample
    |--tidy
        |--file_001-cleaned.csv
        |--file_001-cleaned.rds
|--doc
    |--JohnDoe_2018.pdf
|--analysis
    |--analysis_survey_data.Rmd
|--figures
    |--01-scatterplot.jpg
    |--01-correlation.png
|--R
    |--fun
    |--load_data
    |--plot_data
    |--process_data
        |--filter_data.R
    |--dynamic_document
|--output
    |--model_summary.rds
    |--index_.rds
|--New_R_project.Rproj
```

## Reference

-   [Matthew J. Oldach] https://github.com/moldach/project-directory
-   [A Quick Guide to Organizing Computational Biology
    Projects](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1000424)
-   [Best Practices for Scientific
    Computing](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1001745)
-   [Good Enough Practices in Scientific
    Computing](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005510)
-   [The Biostar Handbook](https://www.biostarhandbook.com/)
-   [Organizing Data Science
    Projects](https://leanpub.com/universities/courses/jhu/cbds-organizing)
-   [Designing
    projects](https://nicercode.github.io/blog/2013-04-05-projects/), by
    [Rich Fitzjohn](https://nicercode.github.io/about/#Team)
-   [How I Start A Bioinformatics
    Project](http://lab.loman.net/2014/05/14/how-i-start-a-bioinformatics-project/),
    by [Nick Lowman](http://lab.loman.net/about/)
-   [Report Writing for Data Science in
    R](https://leanpub.com/reportwriting), by [Roger D.
    Peng](http://www.biostat.jhsph.edu/~rpeng/)
-   [here, here](https://github.com/jennybc/here_here), by [Jenny
    Bryan](https://www.stat.ubc.ca/~jenny/)
-   [here documentation](https://github.com/r-lib/here), by [Kirill
    Muller](https://github.com/krlmlr)
-   [File organization best
    practices](https://andrewbtran.github.io/NICAR/2018/workflow/docs/01-workflow_intro.html?utm_content=buffer858fd&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
    by [Andrew Tran](https://twitter.com/abtran)
-   [ropensci/rrrpkg: tools and templates for making research
    compendia](https://github.com/ropensci/rrrpkg#useful-tools-and-templates-for-making-research-compendia)
-   [How to share data with a
    statistician](https://github.com/jtleek/datasharing) by [Jeff
    Leek](http://jtleek.com/)
-   [Ten Simple Rules for Taking Advantage of
    Github](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004947)

Contact info
----------------------------
Jean-Baptiste Lecomte <.jean.baptiste.lecomte@ifremer.fr>
